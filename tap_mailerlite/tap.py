"""Mailerlite tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th

from tap_mailerlite.streams import (
    MailerliteStream,
    SubscribersStream,
)

STREAM_TYPES = [
    SubscribersStream,
]


class TapMailerlite(Tap):
    """Mailerlite tap class."""

    name = "tap-mailerlite"

    config_jsonschema = th.PropertiesList(
        th.Property("api_key", th.StringType, required=True)
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapMailerlite.cli()
