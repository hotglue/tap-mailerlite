"""REST client handling, including MailerliteStream base class."""

import requests
from pathlib import Path
from typing import Any, Dict, Optional, Iterable


from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import APIKeyAuthenticator
import time


class MailerliteStream(RESTStream):
    """Mailerlite stream class."""

    url_base = "https://connect.mailerlite.com/api"

    records_jsonpath = "$.data[*]"
    next_page_token_jsonpath = "$.meta.current_page"

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = f"Bearer {self.config.get('api_key')}"
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        res = response.json()

        if len(res["data"]) == 0:
            return None

        if "X-RateLimit-Remaining" in response.headers:
            limit = int(float(response.headers["X-RateLimit-Remaining"]))
            if limit <= 2:
                if "Retry-After" in response.headers:
                    sleep_for = int(float(response.headers["Retry-After"]))
                    time.sleep(sleep_for)

        if self.next_page_token_jsonpath:
            all_matches = extract_jsonpath(
                self.next_page_token_jsonpath, response.json()
            )
            first_match = next(iter(all_matches), None)
            next_page_token = first_match
            next_page_token = next_page_token + 1
        else:
            next_page_token = response.headers.get("X-Next-Page", None)

        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token
        if self.replication_key:
            params["sort"] = "asc"
            params["order_by"] = self.replication_key
        return params
